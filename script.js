const content = document.getElementsByClassName('data_sumary');
const list = document.createElement('ul');

fetch('./data.json')
  .then(response => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then(data => {
    return data.map((item, index) => {
      const listItem = document.createElement('li');
      listItem.innerHTML = `
        <div class="dataSummary color-${index % 4}">
          <img src=${item.icon} alt=${item.category}
          <h2>${item.category}</h2>
          <p>${item.score} / <span>100</span></p>
        </div>
      `;
      return listItem;
    });
  })
  .then(listItems => {
    listItems.forEach(item => {
      list.appendChild(item);
    });
    content[0].appendChild(list);
  })
  .catch(err => {
    console.log(err);
  });