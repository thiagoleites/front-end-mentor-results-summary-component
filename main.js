const content = document.getElementsByClassName('box_sumary');
const list = document.createElement('ul');

fetch('./data.json')
  .then(response => response.json())
  .then(data => {

    data.forEach((item) => {
      const listItem = document.createElement('li');
      listItem.innerHTML = `
        <div class="dataSummary">
          <img src=${item.icon} alt=${item.category}
          <h2>${item.category}</h2>
          <p>${item.score} / <span>100</span></p>
        </div>
      `;
      list.appendChild(listItem);
    });
    content[0].appendChild(list);

  }) .catch(err => {
    console.log(err);
  });